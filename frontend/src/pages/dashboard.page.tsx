import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await backendClient.getAllUsers();
        setUsers(result.data);
      } catch (err) {
        console.log({ err })
      } finally {
        setLoading(false)
      }
    };

    fetchData();
  }, []);

  async function handleChangePagination(page: number) {
    try {
      const result = await backendClient.getAllUsers(page);
      setUsers(result.data);
    } catch (err) {
      console.log({ err })
    } finally {
      setLoading(false)
    }
  }

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? (
                <>
                  <div>
                    <button onClick={() => handleChangePagination(1)}>
                      1
                    </button>
                    <button onClick={() => handleChangePagination(2)}>
                      2
                    </button>
                    <button onClick={() => handleChangePagination(3)}>
                      3
                    </button>
                    <button onClick={() => handleChangePagination(4)}>
                      4
                    </button>
                  </div>
                  {users.map((user) => {
                    return <UserCard key={user.id} {...user} />;
                  })}
                </>
              )
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
