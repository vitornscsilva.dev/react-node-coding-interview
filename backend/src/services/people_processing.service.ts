import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getAll(page: number) {
        const startValue = page > 1 ? 50 * (page - 1) : 0;
        return people_data.slice(startValue, 50 * (!page ? 1 : page));
    }
}
